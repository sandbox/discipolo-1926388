<?php
/**
 * @file
 * pushlib_news.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function pushlib_news_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'others reviewed',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '04d641af-4a15-edb4-e559-10d635d43a4a',
    'vocabulary_machine_name' => 'news_types',
  );
  $terms[] = array(
    'name' => 'two cents',
    'description' => 'our opinions on whats going on (in reaction to something)',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '3be9b9a6-1a51-9444-413d-65d16d7def76',
    'vocabulary_machine_name' => 'news_types',
  );
  $terms[] = array(
    'name' => 'We reviewed:',
    'description' => 'here is something we read and what we thought of it',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '8822d488-8364-bdb4-1115-b20852a7a934',
    'vocabulary_machine_name' => 'news_types',
  );
  $terms[] = array(
    'name' => 'advice',
    'description' => 'we share tips',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f27a8cd7-b399-6d04-e55c-ff16d4ff833a',
    'vocabulary_machine_name' => 'news_types',
  );
  return $terms;
}
