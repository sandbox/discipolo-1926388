<?php
/**
 * @file
 * pushlib_news.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pushlib_news_user_default_permissions() {
  $permissions = array();

  // Exported permission: create post content.
  $permissions['create post content'] = array(
    'name' => 'create post content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any post content.
  $permissions['delete any post content'] = array(
    'name' => 'delete any post content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own post content.
  $permissions['delete own post content'] = array(
    'name' => 'delete own post content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete terms in 2.
  $permissions['delete terms in 2'] = array(
    'name' => 'delete terms in 2',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: edit any post content.
  $permissions['edit any post content'] = array(
    'name' => 'edit any post content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own post content.
  $permissions['edit own post content'] = array(
    'name' => 'edit own post content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit terms in 2.
  $permissions['edit terms in 2'] = array(
    'name' => 'edit terms in 2',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
