<?php
/**
 * @file
 * pushlib_news.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pushlib_news_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function pushlib_news_node_info() {
  $items = array(
    'post' => array(
      'name' => t('Post'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
